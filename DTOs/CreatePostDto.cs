using System.ComponentModel.DataAnnotations;

namespace BlogOS.API.Dtos;

public class CreatePostDto
{
    [Required]
    public string Title { get; set; }

    [Required]
    public string Content { get; set; }

    [Required]
    public DateTime PublishedDate { get; set; }

    [Required]
    public string Status { get; set; }
}
