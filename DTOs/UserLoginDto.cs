using System.ComponentModel.DataAnnotations;

namespace BlogOS.API.Models.DTOs
{
    public class UserLoginDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
