using System.ComponentModel.DataAnnotations;

namespace BlogOS.API.Models.DTOs
{
    public class UserRegistrationDto
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
