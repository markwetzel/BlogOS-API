namespace BlogOS.API.Controllers
{
    public class CommentDto
    {
        public Guid CommentId { get; set; }
        public Guid PostId { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
