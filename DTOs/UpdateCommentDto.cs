using System.ComponentModel.DataAnnotations;

namespace BlogOS.API.Controllers
{
    public class UpdateCommentDto
    {
        [Required]
        public string Content { get; set; }
    }
}
