using System.ComponentModel.DataAnnotations;

namespace BlogOS.API.Controllers
{
    public class CreateCommentDto
    {
        [Required]
        public Guid PostId { get; set; }

        [Required]
        public string Content { get; set; }

        [Required]
        public string Author { get; set; }
    }
}
