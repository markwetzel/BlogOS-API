using System.ComponentModel.DataAnnotations;

namespace BlogOS.API.Dtos;

public class UpdatePostDto
{
    [Required]
    public string Title { get; set; }

    [Required]
    public string Content { get; set; }
}
