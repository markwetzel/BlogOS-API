namespace BlogOS.API.Dtos;

public class PostDto
{
    public Guid PostId { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public string Status { get; set; }
}
