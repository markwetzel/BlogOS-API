namespace BlogOS.API.Models;

public interface IBaseService<TDto, TEntity>
    where TEntity : class
{
    Task<TDto> CreateAsync(TDto dto);
    Task<TDto> GetByIdAsync(Guid id);
    Task<IEnumerable<TDto>> GetAllAsync();
    Task UpdateAsync(TDto dto);
    Task DeleteAsync(Guid id);
}
