using BlogOS.API.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BlogOS.API.Data;

public class BlogOSContext : DbContext
{
    public BlogOSContext(DbContextOptions<BlogOSContext> options)
        : base(options) { }

    public DbSet<IdentityUser> Users { get; set; }
    public DbSet<Post> Posts { get; set; }
    public DbSet<Comment> Comments { get; set; }
}
