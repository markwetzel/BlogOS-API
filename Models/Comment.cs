using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BlogOS.API.Models
{
    [Table("Comments")]
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CommentID { get; set; }

        [Required]
        [ForeignKey("Post")]
        public Guid PostID { get; set; }

        [Required]
        public string Content { get; set; }

        public string Author { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        [Required]
        public DateTime UpdatedAt { get; set; }

        public virtual Post Post { get; set; }
    }
}
