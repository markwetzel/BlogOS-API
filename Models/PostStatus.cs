namespace BlogOS.API.Models;

public enum PostStatus
{
    Draft,
    Published,
    Archived,
    Deleted
}
