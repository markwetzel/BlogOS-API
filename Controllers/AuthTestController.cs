using BlogOS.API.Data;
using Microsoft.AspNetCore.Mvc;

namespace BlogOS.API.Controllers;

public class AuthTestController : BaseApiController
{
    public AuthTestController(BlogOSContext context)
        : base(context) { }

    // Endpoint to test authentication
    [HttpGet("test-auth")]
    public IActionResult TestAuthentication()
    {
        return Ok("Authentication successful. You are authorized.");
    }
}
