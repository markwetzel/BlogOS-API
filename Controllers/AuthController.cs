using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BlogOS.API.Data;
using BlogOS.API.Models;
using BlogOS.API.Models.DTOs;
using BlogOS.API.Utilities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace BlogOS.API.Controllers;

public class AuthController : BaseApiController
{
    private readonly IConfiguration _configuration;
    private readonly UserManager<IdentityUser> _userManager;

    public AuthController(
        IConfiguration configuration,
        UserManager<IdentityUser> userManager,
        BlogOSContext context
    )
        : base(context)
    {
        _configuration = configuration;
        _userManager = userManager;
    }

    [HttpPost("register")]
    [AllowAnonymous]
    public async Task<IActionResult> Register([FromBody] UserRegistrationDto registrationDto)
    {
        // Validate incoming data
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        // Check if user already exists
        var userExists = await _userManager.FindByNameAsync(registrationDto.Username);
        if (userExists != null)
        {
            return StatusCode(
                StatusCodes.Status500InternalServerError,
                new Response<object>("Error", "User already exists!")
            );
        }

        User newUser =
            new()
            {
                Email = registrationDto.Email,
                UserName = registrationDto.Username,
                SecurityStamp = Guid.NewGuid().ToString()
            };

        // Create user with supplied password
        var result = await _userManager.CreateAsync(newUser, registrationDto.Password);
        if (!result.Succeeded)
        {
            return StatusCode(
                StatusCodes.Status500InternalServerError,
                new Response<object>("Error", "User creation failed!")
            );
        }

        return Ok(new Response<object>("Success", "User created successfully!") { Data = newUser });
    }

    [HttpPost("login")]
    [AllowAnonymous]
    public async Task<IActionResult> Login([FromBody] UserLoginDto userLoginDto)
    {
        var user = await _userManager.FindByNameAsync(userLoginDto.Username);
        if (user != null && await _userManager.CheckPasswordAsync(user, userLoginDto.Password))
        {
            var authClaims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var authSigningKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes(_configuration["Jwt:Key"])
            );

            var token = new JwtSecurityToken(
                issuer: _configuration["Jwt:Issuer"],
                audience: _configuration["Jwt:Audience"],
                expires: DateTime.Now.AddHours(3),
                claims: authClaims,
                signingCredentials: new SigningCredentials(
                    authSigningKey,
                    SecurityAlgorithms.HmacSha512
                )
            );

            return Ok(
                new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo
                }
            );
        }
        return Unauthorized();
    }
}
