using BlogOS.API.Data;
using BlogOS.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlogOS.API.Controllers
{
    public class CommentsController : BaseApiController
    {
        public CommentsController(BlogOSContext context)
            : base(context) { }

        [HttpPost]
        public async Task<IActionResult> CreateComment([FromBody] CreateCommentDto newComment)
        {
            var comment = new Comment
            {
                PostID = newComment.PostId,
                Content = newComment.Content,
                Author = newComment.Author,
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now
            };

            _context.Comments.Add(comment);
            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetSingleComment),
                new { id = comment.CommentID },
                comment
            );
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteComment(Guid id)
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.CommentID == id);

            if (comment == null)
            {
                return NotFound();
            }

            _context.Comments.Remove(comment);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpGet("post/{postId}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<CommentDto>>> GetCommentsForPost(Guid postId)
        {
            var comments = await _context.Comments
                .Where(c => c.PostID == postId)
                .Select(
                    c =>
                        new CommentDto
                        {
                            CommentId = c.CommentID,
                            PostId = c.PostID,
                            Content = c.Content,
                            Author = c.Author,
                            CreatedAt = c.CreatedAt,
                            UpdatedAt = c.UpdatedAt
                        }
                )
                .ToListAsync();

            return Ok(comments);
        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<CommentDto>> GetSingleComment(Guid id)
        {
            var comment = await _context.Comments
                .Include(c => c.Post)
                .FirstOrDefaultAsync(c => c.CommentID == id);

            if (comment == null)
            {
                return NotFound();
            }

            return new CommentDto
            {
                CommentId = comment.CommentID,
                PostId = comment.PostID,
                Content = comment.Content,
                Author = comment.Author,
                CreatedAt = comment.CreatedAt,
                UpdatedAt = comment.UpdatedAt
            };
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateComment(
            Guid id,
            [FromBody] UpdateCommentDto updatedComment
        )
        {
            var comment = await _context.Comments.FirstOrDefaultAsync(c => c.CommentID == id);

            if (comment == null)
            {
                return NotFound();
            }

            comment.Content = updatedComment.Content;
            comment.UpdatedAt = DateTime.Now;

            await _context.SaveChangesAsync();

            return Ok(comment);
        }
    }
}
