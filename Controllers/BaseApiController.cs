using BlogOS.API.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BlogOS.API.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
public abstract class BaseApiController : ControllerBase
{
    protected readonly BlogOSContext _context;

    protected BaseApiController(BlogOSContext context)
    {
        _context = context;
    }
}
