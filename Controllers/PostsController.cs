using System.Security.Claims;
using BlogOS.API.Data;
using BlogOS.API.Dtos;
using BlogOS.API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BlogOS.API.Controllers;

public class PostsController : BaseApiController
{
    public PostsController(BlogOSContext context)
        : base(context) { }

    [HttpPost]
    public async Task<IActionResult> CreatePost([FromBody] CreatePostDto newPost)
    {
        var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);

        if (userId == null)
        {
            return Unauthorized("User is not authenticated");
        }

        var post = new Post
        {
            UserID = Guid.Parse(userId),
            Title = newPost.Title,
            Content = newPost.Content,
            PublishedDate = newPost.PublishedDate,
            Status = Enum.Parse<PostStatus>(newPost.Status, true),
            CreatedAt = DateTime.Now,
            UpdatedAt = DateTime.Now
        };

        _context.Posts.Add(post);
        await _context.SaveChangesAsync();

        var mappedPost = new PostDto
        {
            PostId = post.PostID,
            Title = post.Title,
            Content = post.Content,
            Status = post.Status.ToString()
        };

        return CreatedAtAction(nameof(GetSinglePost), new { id = post.PostID }, mappedPost);
    }

    [HttpGet]
    [AllowAnonymous]
    public async Task<ActionResult<IEnumerable<PostDto>>> GetAllPosts(
        int page = 1,
        int pageSize = 10
    )
    {
        // Calculate the number of items to skip
        var skip = (page - 1) * pageSize;

        // Query the database for posts, skipping the posts that come before the current page
        // and taking the number of posts specified by the pageSize
        // Lastly, project the results into a PostDto and materialize the query
        var posts = await _context.Posts
            .OrderByDescending(p => p.CreatedAt)
            .Skip(skip)
            .Take(pageSize)
            .Select(
                p =>
                    new PostDto
                    {
                        PostId = p.PostID,
                        Title = p.Title,
                        Content = p.Content
                    }
            )
            .ToListAsync();

        return Ok(posts);
    }

    [HttpGet("{id}")]
    public async Task<IActionResult> GetSinglePost(Guid id)
    {
        var post = await _context.Posts
            .Where(p => p.PostID == id)
            .Select(
                p =>
                    new PostDto
                    {
                        PostId = p.PostID,
                        Title = p.Title,
                        Content = p.Content
                    }
            )
            .FirstOrDefaultAsync();

        if (post == null)
        {
            return NotFound("Post not found");
        }

        return Ok(post);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> UpdatePost(Guid id, [FromBody] UpdatePostDto updatedPost)
    {
        var post = await _context.Posts.FindAsync(id);

        if (post == null)
        {
            return NotFound("Post not found");
        }

        post.Title = updatedPost.Title;
        post.Content = updatedPost.Content;
        post.UpdatedAt = DateTime.Now;

        await _context.SaveChangesAsync();

        // TODO: 204 is returning even if content isn't updated-problem?
        // 204 is typically returned for successful updates
        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePost(Guid id)
    {
        var post = await _context.Posts.FindAsync(id);
        if (post == null)
        {
            return NotFound("Post not found");
        }

        _context.Posts.Remove(post);
        await _context.SaveChangesAsync();

        // 204 is typically returned for successful deletions
        return NoContent();
    }
}
