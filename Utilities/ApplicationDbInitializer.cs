using BlogOS.API.Data;
using BlogOS.API.Models;
using Microsoft.AspNetCore.Identity;

namespace BlogOS.API.Utilities;

public static class ApplicationDbInitializer
{
    public static async Task SeedUsers(UserManager<IdentityUser> userManager)
    {
        // Checking if the admin user already exists
        if (await userManager.FindByNameAsync("admin@blogos.com") == null)
        {
            var adminUser = new IdentityUser
            {
                UserName = "admin@blogos.com",
                Email = "admin@blogos.com",
                EmailConfirmed = true
            };
            await userManager.CreateAsync(adminUser, "AdminPassword123!");
        }

        // Checking if the normal user already exists
        if (await userManager.FindByNameAsync("user@blogos.com") == null)
        {
            var normalUser = new IdentityUser
            {
                UserName = "user@blogos.com",
                Email = "user@blogos.com",
                EmailConfirmed = true
            };
            await userManager.CreateAsync(normalUser, "UserPassword123!");
        }
    }

    public static async Task SeedData(UserManager<IdentityUser> userManager, BlogOSContext context)
    {
        IdentityUser adminUser = await userManager.FindByEmailAsync("admin@blogos.com");
        if (adminUser != null)
        {
            if (!context.Posts.Any())
            {
                var samplePost = new Post
                {
                    PostID = Guid.NewGuid(),
                    UserID = Guid.Parse(adminUser.Id), // Convert from string to Guid
                    Title = "Welcome to BlogOS",
                    Content = "This is the first post on BlogOS.",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow
                };

                context.Posts.Add(samplePost);
                await context.SaveChangesAsync();

                var sampleComment = new Comment
                {
                    CommentID = Guid.NewGuid(),
                    PostID = samplePost.PostID,
                    Content = "Excited to see what comes next!",
                    Author = "System",
                    CreatedAt = DateTime.UtcNow,
                    UpdatedAt = DateTime.UtcNow
                };

                context.Comments.Add(sampleComment);
                await context.SaveChangesAsync();
            }
        }
    }
}
